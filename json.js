{
    "ansible_facts": {
       "ansible_all_ipv4_addresses": [
           "192.168.2.87"
       ],
       "ansible_all_ipv6_addresses": [
           "fe80::fa05:9ef6:f19:5344"
       ],
       "ansible_architecture": "x86_64",
       "ansible_bios_date": "10/27/2016",
       "ansible_bios_version": "F.21",
       "ansible_cmdline": {
           "BOOT_IMAGE": "/boot/vmlinuz-4.10.0-33-generic",
           "quiet": true,
           "ro": true,
           "root": "UUID=dd269658-2293-44da-b5d9-ba70184e6d9c",
           "splash": true,
           "vt.handoff": "7"
       },
       "ansible_date_time": {
           "date": "2018-03-16",
           "day": "16",
           "epoch": "1521215046",
           "hour": "17",
           "iso8601": "2018-03-16T15:44:06Z",
           "iso8601_basic": "20180316T174406040052",
           "iso8601_basic_short": "20180316T174406",
           "iso8601_micro": "2018-03-16T15:44:06.040532Z",
           "minute": "44",
           "month": "03",
           "second": "06",
           "time": "17:44:06",
           "tz": "EET",
           "tz_offset": "+0200",
           "weekday": "Пятница",
           "weekday_number": "5",
           "weeknumber": "11",
           "year": "2018"
       },
       "ansible_default_ipv4": {
           "address": "192.168.2.87",
           "alias": "enp3s0",
           "broadcast": "192.168.3.255",
           "gateway": "192.168.2.250",
           "interface": "enp3s0",
           "macaddress": "40:b0:34:94:57:0a",
           "mtu": 1500,
           "netmask": "255.255.254.0",
           "network": "192.168.2.0",
           "type": "ether"
       },
       "ansible_default_ipv6": {},
       "ansible_devices": {
           "sda": {
               "holders": [],
               "host": "SATA controller: Intel Corporation Device 22a3 (rev 35)",
               "model": "IM2S3138E-128GM-",
               "partitions": {
                   "sda1": {
                       "sectors": "1048576",
                       "sectorsize": 512,
                       "size": "512.00 MB",
                       "start": "2048"
                   },
                   "sda2": {
                       "sectors": "240953344",
                       "sectorsize": 512,
                       "size": "114.90 GB",
                       "start": "1050624"
                   },
                   "sda3": {
                       "sectors": "8065024",
                       "sectorsize": 512,
                       "size": "3.85 GB",
                       "start": "242003968"
                   }
               },
               "removable": "0",
               "rotational": "0",
               "scheduler_mode": "cfq",
               "sectors": "250069680",
               "sectorsize": "512",
               "size": "119.24 GB",
               "support_discard": "512",
               "vendor": "ATA"
           },
           "sr0": {
               "holders": [],
               "host": "SATA controller: Intel Corporation Device 22a3 (rev 35)",
               "model": "DVDRW  DU8AESH",
               "partitions": {},
               "removable": "1",
               "rotational": "1",
               "scheduler_mode": "cfq",
               "sectors": "2097151",
               "sectorsize": "512",
               "size": "1024.00 MB",
               "support_discard": "0",
               "vendor": "hp"
           }
       },
       "ansible_distribution": "Ubuntu",
       "ansible_distribution_major_version": "16",
       "ansible_distribution_release": "xenial",
       "ansible_distribution_version": "16.04",
       "ansible_dns": {
           "nameservers": [
               "127.0.1.1"
           ],
           "search": [
               "dolphi.com.ua"
           ]
       },
       "ansible_domain": "dolphi.com.ua",
       "ansible_enp3s0": {
           "active": true,
           "device": "enp3s0",
           "ipv4": {
               "address": "192.168.2.87",
               "broadcast": "192.168.3.255",
               "netmask": "255.255.254.0",
               "network": "192.168.2.0"
           },
           "ipv6": [
               {
                   "address": "fe80::fa05:9ef6:f19:5344",
                   "prefix": "64",
                   "scope": "link"
               }
           ],
           "macaddress": "40:b0:34:94:57:0a",
           "module": "r8169",
           "mtu": 1500,
           "pciid": "0000:03:00.0",
           "promisc": false,
           "type": "ether"
       },
       "ansible_env": {
           "HOME": "/home/it",
           "LANG": "ru_UA.UTF-8",
           "LANGUAGE": "ru_UA:ru",
           "LC_ALL": "ru_UA.UTF-8",
           "LC_MESSAGES": "ru_UA.UTF-8",
           "LOGNAME": "it",
           "MAIL": "/var/mail/it",
           "PATH": "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games",
           "PWD": "/home/it",
           "SHELL": "/bin/bash",
           "SHLVL": "1",
           "SSH_CLIENT": "192.168.3.175 53428 22",
           "SSH_CONNECTION": "192.168.3.175 53428 192.168.2.87 22",
           "SSH_TTY": "/dev/pts/2",
           "TERM": "xterm-256color",
           "USER": "it",
           "XDG_RUNTIME_DIR": "/run/user/1000",
           "XDG_SESSION_ID": "5",
           "_": "/usr/bin/python",
           "ftp_proxy": "ftp://bsd.dolphi.com.ua:3128/",
           "http_proxy": "http://bsd.dolphi.com.ua:3128/",
           "https_proxy": "https://bsd.dolphi.com.ua:3128/",
           "socks_proxy": "socks://bsd.dolphi.com.ua:3128/"
       },
       "ansible_fips": false,
       "ansible_form_factor": "Notebook",
       "ansible_fqdn": "rudenko-a.dolphi.com.ua",
       "ansible_hostname": "rudenko-a",
       "ansible_interfaces": [
           "wlp2s0",
           "enp3s0",
           "lo"
       ],
       "ansible_kernel": "4.10.0-33-generic",
       "ansible_lo": {
           "active": true,
           "device": "lo",
           "ipv4": {
               "address": "127.0.0.1",
               "broadcast": "host",
               "netmask": "255.0.0.0",
               "network": "127.0.0.0"
           },
           "ipv6": [
               {
                   "address": "::1",
                   "prefix": "128",
                   "scope": "host"
               }
           ],
           "mtu": 65536,
           "promisc": false,
           "type": "loopback"
       },
       "ansible_lsb": {
           "codename": "xenial",
           "description": "Ubuntu 16.04.4 LTS",
           "id": "Ubuntu",
           "major_release": "16",
           "release": "16.04"
       },
       "ansible_machine": "x86_64",
       "ansible_machine_id": "275ac0b116bd4442bec433e9a9180962",
       "ansible_memfree_mb": 124,
       "ansible_memory_mb": {
           "nocache": {
               "free": 1190,
               "used": 2604
           },
           "real": {
               "free": 124,
               "total": 3794,
               "used": 3670
           },
           "swap": {
               "cached": 0,
               "free": 3934,
               "total": 3937,
               "used": 3
           }
       },
       "ansible_memtotal_mb": 3794,
       "ansible_mounts": [
           {
               "device": "/dev/sda2",
               "fstype": "ext4",
               "mount": "/",
               "options": "rw,relatime,errors=remount-ro,data=ordered",
               "size_available": 108880429056,
               "size_total": 121296424960,
               "uuid": "dd269658-2293-44da-b5d9-ba70184e6d9c"
           },
           {
               "device": "/dev/sda1",
               "fstype": "vfat",
               "mount": "/boot/efi",
               "options": "rw,relatime,fmask=0077,dmask=0077,codepage=437,iocharset=iso8859-1,shortname=mixed,errors=remount-ro",
               "size_available": 532221952,
               "size_total": 535805952,
               "uuid": "8C80-99E3"
           }
       ],
       "ansible_nodename": "rudenko-a",
       "ansible_os_family": "Debian",
       "ansible_pkg_mgr": "apt",
       "ansible_processor": [
           "GenuineIntel",
           "Intel® Celeron® CPU  N3060  @ 1.60GHz",
           "GenuineIntel",
           "Intel® Celeron® CPU  N3060  @ 1.60GHz"
       ],
       "ansible_processor_cores": 2,
       "ansible_processor_count": 1,
       "ansible_processor_threads_per_core": 1,
       "ansible_processor_vcpus": 2,
       "ansible_product_name": "HP 250 G5 Notebook PC",
       "ansible_product_serial": "NA",
       "ansible_product_uuid": "NA",
       "ansible_product_version": "Type1ProductConfigId",
       "ansible_python_version": "2.7.12",
       "ansible_selinux": false,
       "ansible_service_mgr": "systemd",
       "ansible_ssh_host_key_dsa_public": "AAAAB3NzaC1kc3MAAACBAJD9/SOBeDHuWQ9qPLTprXVDZpv1lTZMsQRQz2eT3Qq/9xEOPm7aF3XeJvmiG3O4C5Rr24A1+50KyTjLG6dbnJd2Un1IJbaOlMSuQ/H1q64hEz0kklTvPqz6/3l5ii8ePBFMdGKVAXpQW/wI9i5bWoFYlYZTH2CIcNKwxcx6IbSTAAAAFQCaCtF9B+zF0RYGumJ76gWxFwucGQAAAIEAhvfaeNonbcSvIOXe2c3aziT4ydsXcez/No+e7d3zkryMDKQ9+qefMjnzacD+axqIvQOgoUUdHx2EEIVmGl8QjgH7HjOmpW7//N2lDBErZ7hWzKr0FJuwlVkXEXFqiCydZpT+UFo8ztsuHEW4uJYE1X4Gs7bSvasb7sfE3gWwbrYAAACAWQx1GIxF2M6GUaniEn+IEyOwFJ8CpmFaoTzsBNMEnEylf3eUTymvizXGHoFZeconvMuQuyalprtttxbOpUGZZN6NJAqV7eEc/07o9DJJMsY73GEsxsgx4kRLiI9szwQp5hd/KZ6nj4QS842HErUuyIPwNuiUc+2x29JvY0kBbMo=",
       "ansible_ssh_host_key_ecdsa_public": "AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBBlTgmglEHQxTaXgn4G1ILMd5UfNy2iotY7W0HiXdmUaMkZnr5m6/jFRoxn3VuOllrR8S5Wg0OeHVu6cq1LDFW4=",
       "ansible_ssh_host_key_ed25519_public": "AAAAC3NzaC1lZDI1NTE5AAAAILhUYbvDn3dFU5M5+YxM88mwisKEnE5IlKL0tdCoMU02",
       "ansible_ssh_host_key_rsa_public": "AAAAB3NzaC1yc2EAAAADAQABAAABAQDGnHuUiL/PYsE2vi8EWu+jpN/7603ml6JELIiyzb25YLyhOvEMX7WQuFGI6KLw6/JZ2IcRMT8NG7S4BDsw4Iu5rZlQCIIfLMsqw/Pc7odAdIdngeDRh+eodWGZD2JpUeOge9PdYMtdK7vbDLEYLfQcZZHd0kja1SEvRh/m3R5X5rBI/6Vl6L0zn1YicnUlvh8jTGuHCSaXQom4UX1YWUF852nHEyB2Ey6BuA41QdOAmqey1jvG5UqpDtLuwePe9PLYDO1J19Uk+p0hY/23iDdh+0MroBNAGGkZJ25J10tBumDjWjPVs/4hyYv77mKfl/Bw6NGtM78Xzd9pZ0TfWSPR",
       "ansible_swapfree_mb": 3934,
       "ansible_swaptotal_mb": 3937,
       "ansible_system": "Linux",
       "ansible_system_vendor": "HP",
       "ansible_uptime_seconds": 5480,
       "ansible_user_dir": "/home/it",
       "ansible_user_gecos": "it,,,",
       "ansible_user_gid": 1000,
       "ansible_user_id": "it",
       "ansible_user_shell": "/bin/bash",
       "ansible_user_uid": 1000,
       "ansible_userspace_architecture": "x86_64",
       "ansible_userspace_bits": "64",
       "ansible_virtualization_role": "host",
       "ansible_virtualization_type": "kvm",
       "ansible_wlp2s0": {
           "active": false,
           "device": "wlp2s0",
           "macaddress": "98:54:1b:c4:d1:b4",
           "module": "iwlwifi",
           "mtu": 1500,
           "pciid": "0000:02:00.0",
           "promisc": false,
           "type": "ether"
       },
       "module_setup": true
   },
   "changed": false,
   "invocation": {
       "module_args": {
           "fact_path": "/etc/ansible/facts.d",
           "filter": "*"
       },
       "module_name": "setup"
   }
}